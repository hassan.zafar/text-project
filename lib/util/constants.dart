import 'package:flutter/material.dart';

class Images {
  static const String agreement = 'assets/images/agreement.png';
  static const String profileData = 'assets/images/Profile_data.png';
  static const String task = 'assets/images/task.png';
  static const String personalFile = 'assets/images/personal_file.png';
  static const String rightLine = 'assets/images/right_line.png';
  static const String leftLine = 'assets/images/left_line.png';
  static const String aboutMe = 'assets/images/about_me.png';
  static const String swipeProfiles = 'assets/images/swipe_profiles.png';
  static const String jobOffer = 'assets/images/job_offers.png';
  static const String businessDeal = 'assets/images/business_deal.png';
}

class ThemeColors {
  static const aquaGreen = Color(0xff81E6D9);
  static const lightSeaGreen = Color(0xff319795);
  static const slateGray = Color(0xff718096);
  static const darkSlateGray = Color(0xff2D3748);
  static const dimGray = Color(0xff4A5568);
  static const ghostWhite = Color(0xffF7FAFC);
}
