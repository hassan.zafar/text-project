import 'package:flutter/material.dart';

class StylesUtils {
  static const EdgeInsets tabPadding = EdgeInsets.all(16.0);
  static const double avatarRadius = 66.0;

  static BoxDecoration tabDecoration = BoxDecoration(
    border: Border.all(color: const Color(0xff81E6D9)),
    color: const Color(0xff81E6D9),
  );
  static const corners = Radius.circular(20);

  static const BoxDecoration curveGradientDecoration = BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [
        Color(0xFFE6FFFA),
        Color(0xFFEBF4FF),
      ],
    ),
  );
  static const BoxDecoration invertCurveGradientDecoration = BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [
        Color(0xFFEBF4FF),
        Color(0xFFE6FFFA),
      ],
    ),
  );
  static const LinearGradient jobGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      Color(0xFFEBF4FF),
      Color(0xFFE6FFFA),
    ],
  );
  static const LinearGradient registerGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF319795),
      Color(0xFF3182CE),
    ],
  );
  static const LinearGradient appbarGradient = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF319795),
      Color(0xFF3182CE),
    ],
  );
}
