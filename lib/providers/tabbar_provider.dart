import 'package:flutter/material.dart';

class TabbarProvider extends ChangeNotifier {
  bool _isSelected = false;

  bool get isSelected => _isSelected;
  String _id = 'Arbeitnehmer';
  String get id => _id;


  void isSelectedOption(String thisId) {
    if (thisId == _id) {
      _id = '';
      _isSelected = false;
    } else {
      _id = thisId;
      _isSelected = true;
    }
    notifyListeners();
  }
}