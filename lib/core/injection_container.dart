import 'package:get_it/get_it.dart' show GetIt;
import 'package:test/providers/tabbar_provider.dart';

class Di {
  static final GetIt sl = GetIt.instance;
  static Future<void> init() async {
    sl.registerLazySingleton<TabbarProvider>(() => TabbarProvider());
  }
}
