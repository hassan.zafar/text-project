import 'package:flutter/material.dart';
import 'package:provider/provider.dart' show ChangeNotifierProvider, Consumer;
import 'package:test/core/injection_container.dart';

class BaseView<T extends ChangeNotifier> extends StatefulWidget {
  const BaseView({Key? key, required this.builder, this.onModelReady})
      : super(key: key);
  final Widget Function(BuildContext context, T value, Widget? child) builder;
  final Function(T)? onModelReady;

  @override
  BaseViewState<T> createState() => BaseViewState<T>();
}

class BaseViewState<T extends ChangeNotifier> extends State<BaseView<T>> {
  T? model = Di.sl<T>();

  @override
  void initState() {
    if (widget.onModelReady != null) {
      widget.onModelReady!(model!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>.value(
      value: Di.sl<T>(),
      child: Consumer<T>(builder: widget.builder),
    );
  }
}
