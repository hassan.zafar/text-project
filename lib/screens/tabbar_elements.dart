import 'package:flutter/material.dart';
import 'package:test/core/base_view.dart';
import 'package:test/providers/tabbar_provider.dart';
import 'package:test/util/constants.dart';
import 'package:test/util/styles.dart';
import 'package:test/widgets/clipper_widgets.dart';

class TabBarWidget extends StatelessWidget {
  const TabBarWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BaseView<TabbarProvider>(builder: (context, tabbarProvider, _) {
      switch (tabbarProvider.id) {
        case 'Arbeitnehmer':
          return arbeitnehmerTab(context);
        case 'Arbeitgeber':
          return arbeitgeberTab(context);
        case 'Temporärbüro':
          return temporarburoTab(context);
        default:
          return arbeitnehmerTab(context);
      }
    });
  }

  Widget arbeitnehmerTab(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return size.width > 800
        ? Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 20),
                    child: Text(
                      'Drei einfache Schritte\n zu deinem neuen Job',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '1.',
                              style: TextStyle(
                                fontSize: 130,
                              ),
                            ),
                            Text(
                              '\n\n\nErstellen dein Lebenslauf',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(width: 100),
                        Image.asset(Images.profileData, height: 180),
                      ],
                    ),
                  ),
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        children: [
                          ClipPath(
                            clipper: WaveClipper(),
                            child: Container(
                              height: 150,
                              decoration: StylesUtils.curveGradientDecoration,
                            ),
                          ),
                          Container(
                            height: 300,
                            decoration: StylesUtils.curveGradientDecoration,
                          ),
                          ClipPath(
                            clipper: WaveClipperInvert(),
                            child: Container(
                              height: 150,
                              decoration:
                                  StylesUtils.curveGradientDecoration,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(Images.task, height: 126),
                          const SizedBox(
                            width: 100,
                          ),
                          RichText(
                            maxLines: 2,
                            softWrap: true,
                            text: TextSpan(
                              style: Theme.of(context).textTheme.bodyMedium,
                              children: const <TextSpan>[
                                TextSpan(
                                    text: '2.',
                                    style: TextStyle(
                                      fontSize: 130,
                                    )),
                                TextSpan(
                                    text: 'Erstellen dein Lebenslauf',
                                    style: TextStyle(
                                      fontSize: 18,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Stack(
                    clipBehavior: Clip.none,
                    children: [
                      const Positioned(
                        left: 380,
                        top: 20,
                        child: CircleAvatar(
                            radius: 120,
                            backgroundColor: ThemeColors.ghostWhite),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '3.',
                                style: TextStyle(
                                  fontSize: 130,
                                ),
                              ),
                              Text(
                                '\n\nMit nur einem Klick bewerben',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(width: 80),
                          Image.asset(Images.personalFile, height: 210),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Positioned(
                  top: 260,
                  left: size.width * .3,
                  child: Image.asset(
                    Images.rightLine,
                    width: 350,
                  )),
              Positioned(
                  bottom: 180,
                  left: size.width * .28,
                  child: Image.asset(
                    Images.leftLine,
                  )),
            ],
          )
        : Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: Text(
                  'Drei einfache Schritte\n zu deinem neuen Job',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(Images.profileData, height: 180),
                        const SizedBox(
                          height: 100,
                        ),
                      ],
                    ),
                    const Positioned(
                      bottom: 10,
                      left: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                              fontSize: 130,
                            ),
                          ),
                          Text(
                            '\n\n\nErstellen dein Lebenslauf',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      ClipPath(
                        clipper: WaveClipper(),
                        child: Container(
                          height: 150,
                          decoration: StylesUtils.curveGradientDecoration,
                        ),
                      ),
                      Container(
                        height: 300,
                        decoration: StylesUtils.curveGradientDecoration,
                      ),
                      Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.identity()
                          ..scale(-1.0, 1.0), // This flips it horizontally
                        child: ClipPath(
                          clipper: WaveClipperInvert(),
                          child: Container(
                            height: 150,
                            decoration:
                                StylesUtils.invertCurveGradientDecoration,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 70),
                      Image.asset(Images.task, height: 126),
                    ],
                  ),
                  Positioned(
                    top: 100,
                    left: 40,
                    child: RichText(
                      maxLines: 2,
                      softWrap: true,
                      text: TextSpan(
                        style: Theme.of(context).textTheme.bodyMedium,
                        children: const <TextSpan>[
                          TextSpan(
                              text: '2.',
                              style: TextStyle(
                                fontSize: 130,
                              )),
                          TextSpan(
                              text: 'Erstellen dein Lebenslauf',
                              style: TextStyle(
                                fontSize: 18,
                              )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  const Positioned(
                    left: -200,
                    top: -100,
                    child: CircleAvatar(
                        radius: 200, backgroundColor: ThemeColors.ghostWhite),
                  ),
                  Column(
                    children: [
                      const SizedBox(height: 150),
                      Image.asset(Images.personalFile, height: 210),
                    ],
                  ),
                  const Positioned(
                    top: 0,
                    left: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '3.',
                          style: TextStyle(
                            fontSize: 130,
                          ),
                        ),
                        Text(
                          '\n\nMit nur einem Klick\n bewerben',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
  }

  Widget arbeitgeberTab(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return size.width > 800
        ? Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 20),
                    child: Text(
                      'Drei einfache Schritte\n zu deinem neuen Mitarbeiter',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '1.',
                              style: TextStyle(
                                fontSize: 130,
                              ),
                            ),
                            Text(
                              '\n\nErstellen dein Unternehmensprofil',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(width: 20),
                        Image.asset(Images.profileData, height: 180),
                      ],
                    ),
                  ),
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        children: [
                          ClipPath(
                            clipper: WaveClipper(),
                            child: Container(
                              height: 150,
                              decoration: StylesUtils.curveGradientDecoration,
                            ),
                          ),
                          Container(
                            height: 250,
                            decoration: StylesUtils.curveGradientDecoration,
                          ),
                          ClipPath(
                            clipper: WaveClipperInvert(),
                            child: Container(
                              height: 150,
                              decoration:
                                  StylesUtils.curveGradientDecoration,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(Images.aboutMe),
                          const SizedBox(
                            width: 100,
                          ),
                          RichText(
                            maxLines: 2,
                            softWrap: true,
                            text: TextSpan(
                              style: Theme.of(context).textTheme.bodyMedium,
                              children: const <TextSpan>[
                                TextSpan(
                                    text: '2.',
                                    style: TextStyle(
                                      fontSize: 130,
                                    )),
                                TextSpan(
                                    text: 'Erstellen ein Jobinserat',
                                    style: TextStyle(
                                      fontSize: 18,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Stack(
                    children: [
                      const Positioned(
                        left: 380,
                        child: CircleAvatar(
                            radius: 120,
                            backgroundColor: ThemeColors.ghostWhite),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Row(
                            children: [
                              Text(
                                '3.',
                                style: TextStyle(
                                  fontSize: 130,
                                ),
                              ),
                              Text(
                                '\n\nWähle deinen neuen Mitarbeiter aus',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(width: 40),
                          Image.asset(Images.swipeProfiles),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Positioned(
                  top: 260,
                  left: size.width * .31,
                  child: Image.asset(
                    Images.rightLine,
                    width: 350,
                  )),
              Positioned(
                  bottom: 180,
                  left: size.width * 0.3,
                  child: Image.asset(Images.leftLine)),
            ],
          )
        : Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: Text(
                  'Drei einfache Schritte\n zu deinem neuen Mitarbeiter',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(Images.profileData, height: 180),
                        const SizedBox(
                          height: 100,
                        ),
                      ],
                    ),
                    const Positioned(
                      bottom: 10,
                      left: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                              fontSize: 130,
                            ),
                          ),
                          Text(
                            '\n\nErstellen dein \nUnternehmensprofil',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      ClipPath(
                        clipper: WaveClipper(),
                        child: Container(
                          height: 150,
                          decoration: StylesUtils.curveGradientDecoration,
                        ),
                      ),
                      Container(
                        height: 250,
                        decoration: StylesUtils.curveGradientDecoration,
                      ),
                      Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.identity()
                          ..scale(-1.0, 1.0), // This flips it horizontally
                        child: ClipPath(
                          clipper: WaveClipperInvert(),
                          child: Container(
                            height: 150,
                            decoration:
                                StylesUtils.invertCurveGradientDecoration,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 70),
                      Image.asset(Images.aboutMe),
                    ],
                  ),
                  Positioned(
                    top: 120,
                    left: 40,
                    child: RichText(
                      maxLines: 2,
                      softWrap: true,
                      text: TextSpan(
                        style: Theme.of(context).textTheme.bodyMedium,
                        children: const <TextSpan>[
                          TextSpan(
                              text: '2.',
                              style: TextStyle(
                                fontSize: 130,
                              )),
                          TextSpan(
                              text: 'Erstellen ein Jobinserat',
                              style: TextStyle(
                                fontSize: 18,
                              )),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  const Positioned(
                    left: -200,
                    top: -100,
                    child: CircleAvatar(
                        radius: 200, backgroundColor: ThemeColors.ghostWhite),
                  ),
                  Column(
                    children: [
                      const SizedBox(height: 150),
                      Image.asset(Images.aboutMe),
                    ],
                  ),
                  const Positioned(
                    top: 0,
                    left: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '3.',
                          style: TextStyle(
                            fontSize: 130,
                          ),
                        ),
                        Text(
                          '\n\nWähle deinen\n neuen Mitarbeiter aus',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
  }

  Widget temporarburoTab(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return size.width > 800
        ? Stack(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 20),
                    child: Text(
                      'Drei einfache Schritte zur\n Vermittlung neuer Mitarbeiter',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                              fontSize: 130,
                            ),
                          ),
                          Text(
                            '\n\nErstellen dein Unternehmensprofil',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 20),
                      Image.asset(Images.profileData, height: 180),
                    ],
                  ),
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Column(
                        children: [
                          ClipPath(
                            clipper: WaveClipper(),
                            child: Container(
                              height: 150,
                              decoration: StylesUtils.curveGradientDecoration,
                            ),
                          ),
                          Container(
                            height: 250,
                            decoration: StylesUtils.curveGradientDecoration,
                          ),
                          ClipPath(
                            clipper: WaveClipperInvert(),
                            child: Container(
                              height: 150,
                              decoration:
                                  StylesUtils.curveGradientDecoration,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(Images.jobOffer),
                          const SizedBox(
                            width: 100,
                          ),
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '2.',
                                style: TextStyle(
                                  fontSize: 130,
                                ),
                              ),
                              Text(
                                '\nErhalte Vermittlungs-\n angebot von Arbeitgeber',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  Stack(
                    clipBehavior: Clip.none,
                    children: [
                      const Positioned(
                        left: 380,
                        child: CircleAvatar(
                            radius: 120,
                            backgroundColor: ThemeColors.ghostWhite),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '3.',
                                style: TextStyle(
                                  fontSize: 130,
                                ),
                              ),
                              Text(
                                '\nVermittlung nach Provision oder Stundenlohn',
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                          Image.asset(Images.businessDeal),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Positioned(
                  top: 260,
                  left: size.width * .31,
                  child: Image.asset(
                    Images.rightLine,
                    width: 350,
                  )),
              Positioned(
                  bottom: 180, left: 420, child: Image.asset(Images.leftLine)),
            ],
          )
        : Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: Text(
                  'Drei einfache Schritte zur\n Vermittlung neuer Mitarbeiter',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(Images.profileData, height: 180),
                        const SizedBox(
                          height: 100,
                        ),
                      ],
                    ),
                    const Positioned(
                      bottom: 10,
                      left: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '1.',
                            style: TextStyle(
                              fontSize: 130,
                            ),
                          ),
                          Text(
                            '\n\nErstellen dein \nUnternehmensprofil',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      ClipPath(
                        clipper: WaveClipper(),
                        child: Container(
                          height: 150,
                          decoration: StylesUtils.curveGradientDecoration,
                        ),
                      ),
                      Container(
                        height: 250,
                        decoration: StylesUtils.curveGradientDecoration,
                      ),
                      Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.identity()
                          ..scale(-1.0, 1.0), // This flips it horizontally
                        child: ClipPath(
                          clipper: WaveClipperInvert(),
                          child: Container(
                            height: 150,
                            decoration:
                                StylesUtils.invertCurveGradientDecoration,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 70),
                      Image.asset(Images.jobOffer),
                    ],
                  ),
                  const Positioned(
                    top: 100,
                    left: 40,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '2.',
                          style: TextStyle(
                            fontSize: 130,
                          ),
                        ),
                        Text(
                          '\nErhalte Vermittlungs-\n angebot von Arbeitgeber',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  const Positioned(
                    left: -200,
                    top: -100,
                    child: CircleAvatar(
                        radius: 200, backgroundColor: ThemeColors.ghostWhite),
                  ),
                  Column(
                    children: [
                      const SizedBox(height: 150),
                      Image.asset(Images.businessDeal),
                    ],
                  ),
                  const Positioned(
                    top: 0,
                    left: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '3.',
                          style: TextStyle(
                            fontSize: 130,
                          ),
                        ),
                        Text(
                          '\nVermittlung nach\n Provision oder\n Stundenlohn',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          );
  }
}
