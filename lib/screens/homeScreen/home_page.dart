import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test/screens/tabbar_elements.dart';
import 'package:test/util/constants.dart';
import 'package:test/util/styles.dart';
import 'package:test/widgets/clipper_widgets.dart';
import 'package:test/widgets/custom_appbar.dart';
import 'package:test/widgets/tabbar.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      body: size.width > 800
          ? Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(height: 80),
                      Container(
                        padding: const EdgeInsets.all(32.0),
                        decoration: const BoxDecoration(
                          gradient: StylesUtils.jobGradient,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Deine Job \n Website',
                                    style:
                                        Theme.of(context).textTheme.titleLarge),
                                const SizedBox(height: 20),
                                Container(
                                  width: 320,
                                  height: 40,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    gradient: StylesUtils.registerGradient,
                                  ),
                                  child: const Center(
                                      child: Text(
                                    'Kostenlos Registrieren',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white),
                                  )),
                                )
                              ],
                            ),
                            const SizedBox(width: 100),
                            const CircleAvatar(
                              radius: 130,
                              backgroundColor: Colors.white,
                              backgroundImage: AssetImage(Images.agreement),
                            )
                          ],
                        ),
                      ),
                      ClipPath(
                        clipper: WaveClipperInvert(),
                        child: Container(
                          height: 150,
                          decoration: const BoxDecoration(
                            gradient: StylesUtils.jobGradient,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 100,
                      ),
                      const CustomTabBar(),
                      const TabBarWidget(),
                      const SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                ),
                const CustomAppBar(),
              ],
            )
          : Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(height: 80),
                      Container(
                        decoration: const BoxDecoration(
                          gradient: StylesUtils.jobGradient,
                        ),
                        child: Column(
                          children: [
                            const Text('Deine Job \n Website',
                                style: TextStyle(fontSize: 42)),
                            Image.asset(Images.agreement),
                          ],
                        ),
                      ),
                      ClipPath(
                        clipper: WaveClipperInvert(),
                        child: Container(
                          height: 150,
                          decoration: const BoxDecoration(
                            gradient: StylesUtils.jobGradient,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const CustomTabBar(),
                      const TabBarWidget(),
                      const SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          topLeft: StylesUtils.corners,
                          topRight: StylesUtils.corners),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: const Offset(3, 0),
                        ),
                      ],
                    ),
                    padding: const EdgeInsets.all(32.0),
                    child: Container(
                      width: 320,
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: StylesUtils.registerGradient,
                      ),
                      child: const Center(
                          child: Text(
                        'Kostenlos Registrieren',
                        style: TextStyle(fontSize: 14, color: Colors.white),
                      )),
                    ),
                  ),
                ),
                const CustomAppBar()
              ],
            ),
    ));
  }
}
