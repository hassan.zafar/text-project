import 'package:flutter/material.dart';
import 'package:test/core/injection_container.dart';
import 'package:test/screens/homeScreen/home_page.dart';
import 'package:test/util/constants.dart';

void main() {
  Di.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Lato',
        textTheme: TextTheme(
          displayLarge: const TextStyle(),
          titleMedium: const TextStyle(fontSize: 21).apply(
            color: ThemeColors.darkSlateGray,
          ),
          titleLarge:
              const TextStyle(fontSize: 42, fontWeight: FontWeight.w900).apply(
            color: ThemeColors.dimGray,
          ),
          bodyMedium: const TextStyle().apply(
            color: ThemeColors.slateGray,
          ),
          bodySmall: const TextStyle().apply(
            color: ThemeColors.lightSeaGreen,
          ),
        ),
        colorScheme: ColorScheme.fromSeed(
          seedColor: ThemeColors.slateGray,
        ),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}
