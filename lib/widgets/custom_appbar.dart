import 'package:flutter/material.dart';
import 'package:test/util/constants.dart';
import 'package:test/util/styles.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      child: SizedBox(
        height: 80,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Container(
              height: 10,
              decoration: const BoxDecoration(
                gradient: StylesUtils.appbarGradient,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 3,
                      offset: const Offset(0, 0), // changes position of shadow
                    ),
                  ],
                  borderRadius: const BorderRadius.only(
                      bottomLeft: StylesUtils.corners,
                      bottomRight: StylesUtils.corners)),
              child: const Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(24.0),
                      child: Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 14, color: ThemeColors.lightSeaGreen),
                      ),
                    )
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
