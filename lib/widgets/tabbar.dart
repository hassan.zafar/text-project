import 'package:flutter/material.dart';
import 'package:test/core/base_view.dart';
import 'package:test/providers/tabbar_provider.dart';
import 'package:test/util/constants.dart';
import 'package:test/util/styles.dart';

class CustomTabBar extends StatelessWidget {
  const CustomTabBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BaseView<TabbarProvider>(builder: (context, tabbarProvider, _) {
      return const Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TabBarElement(
              title: 'Arbeitnehmer',
              borderRadius: BorderRadius.only(
                topLeft: StylesUtils.corners,
                bottomLeft: StylesUtils.corners,
              )),
          TabBarElement(
            title: 'Arbeitgeber',
          ),
          TabBarElement(
            title: 'Temporärbüro',
            borderRadius: BorderRadius.only(
              topRight: StylesUtils.corners,
              bottomRight: StylesUtils.corners,
            ),
          ),
        ],
      );
    });
  }
}

class TabBarElement extends StatelessWidget {
  const TabBarElement({super.key, required this.title, this.borderRadius});
  final String title;
  final BorderRadius? borderRadius;
  @override
  Widget build(BuildContext context) {
    return BaseView<TabbarProvider>(builder: (context, tabbarProvider, _) {
      return GestureDetector(
        onTap: () {
          tabbarProvider.isSelectedOption(title);
        },
        child: Container(
          decoration: StylesUtils.tabDecoration.copyWith(
            color: tabbarProvider.id == title
                ? ThemeColors.aquaGreen
                : Colors.white,
            borderRadius: borderRadius ?? const BorderRadius.only(),
          ),
          padding: StylesUtils.tabPadding,
          child: Text(title,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    color: tabbarProvider.id == title
                        ? Colors.white
                        : ThemeColors.aquaGreen,
                  )),
        ),
      );
    });
  }
}
