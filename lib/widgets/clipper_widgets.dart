import 'dart:ui';

import 'package:flutter/material.dart';

class WaveClipperInvert extends CustomClipper<Path> {
  WaveClipperInvert();

  @override
  Path getClip(Size size) {
    final path = Path();
    final startY = size.height / 2;

    path.lineTo(0, startY);
    path.quadraticBezierTo(
      size.width * 0.25,
      startY - 40,
      size.width * 0.5,
      startY,
    );
    path.quadraticBezierTo(
      size.width * 0.75,
      startY + 40,
      size.width,
      startY,
    );
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class WaveClipper extends CustomClipper<Path> {
  final bool invert;

  WaveClipper({this.invert = false});

  @override
  Path getClip(Size size) {
    final path = Path();
    final startY = size.height * 0.7;
    final controlPointY = size.height * 0.3;

    path.moveTo(0, startY);
    path.quadraticBezierTo(
      size.width * 0.25,
      startY + controlPointY,
      size.width * 0.5,
      startY,
    );
    path.quadraticBezierTo(
      size.width * 0.75,
      startY - controlPointY,
      size.width,
      startY,
    );

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

